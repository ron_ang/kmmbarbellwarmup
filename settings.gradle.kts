pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "Warm_Up_Reps_and_Sets"
include(":androidWarmup")
include(":warmupFramework")