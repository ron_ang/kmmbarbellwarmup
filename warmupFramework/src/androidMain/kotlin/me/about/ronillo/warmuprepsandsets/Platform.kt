package me.about.ronillo.warmuprepsandsets

actual class Platform actual constructor() {
    actual val platform: String = "Android ${android.os.Build.VERSION.SDK_INT}"

    actual fun format(num: Float, digits: ULong): String? {
        return "%.${digits}f".format(num)
    }
}
