package me.about.ronillo.warmuprepsandsets

import platform.Foundation.NSNumber
import platform.Foundation.NSNumberFormatter
import platform.UIKit.UIDevice

actual class Platform actual constructor() {

    actual val platform: String =
        UIDevice.currentDevice.systemName() +
                " " + UIDevice.currentDevice.systemVersion

    actual fun format(num: Float, digits: ULong): String? {
        val formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = digits
        formatter.maximumFractionDigits = digits
        return formatter.stringFromNumber(NSNumber(num))
    }
}
