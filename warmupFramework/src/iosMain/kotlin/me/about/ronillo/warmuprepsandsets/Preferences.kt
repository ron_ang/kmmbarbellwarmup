package me.about.ronillo.warmuprepsandsets

import platform.Foundation.NSUserDefaults
import platform.Foundation.setValue
import platform.darwin.NSObject

actual typealias SPref = NSObject

actual fun SPref.get(key: String) : String {
    return NSUserDefaults.standardUserDefaults.stringForKey(key).toString()
}

actual fun SPref.set(key: String, value: String){
    NSUserDefaults.standardUserDefaults.setValue(value = value, forKey = key)
}
