package me.about.ronillo.warmuprepsandsets

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

object ProgramSerializer {

    fun deserialize(jsonString: String) : Program {
        val json = Json {
            ignoreUnknownKeys = true
            isLenient = true
            prettyPrint = true
        }
        return json.decodeFromString(jsonString)
    }
}
