package me.about.ronillo.warmuprepsandsets

expect class Platform() {

    val platform: String

    fun format(num: Float, digits: ULong) : String?
}