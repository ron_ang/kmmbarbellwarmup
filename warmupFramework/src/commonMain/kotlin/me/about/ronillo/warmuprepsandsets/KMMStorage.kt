package me.about.ronillo.warmuprepsandsets

class KMMStorage(val context: SPref) {

    fun get(key: String): String {
        return context.get(key)
    }

    fun put(key: String, value: String) {
        context.set(key,value)
    }
}
