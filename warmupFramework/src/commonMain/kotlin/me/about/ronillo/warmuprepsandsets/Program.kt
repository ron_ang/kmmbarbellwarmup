package me.about.ronillo.warmuprepsandsets

import kotlinx.serialization.Serializable
import me.about.ronillo.warmuprepsandsets.parcel.Parcelable

const val PROGRAM_5X5_JSON = """
    {
  "title" : "5x5",
  "exercises" : [{
    "name" : "Squat",
    "max" : 600,
    "workouts" : [{
        "sets" : "2",
        "reps" : "5",
        "multiplier" : 0
      }, {
        "sets" : "1",
        "reps" : "5",
        "multiplier" : 0.4
      }, {
        "sets" : "1",
        "reps" : "3",
        "multiplier" : 0.6
      }, {
        "sets" : "1",
        "reps" : "2",
        "multiplier" : 0.8
      }, {
        "sets" : "5",
        "reps" : "5",
        "multiplier" : 1.0
      }]
    },{
    "name" : "Bench Press",
    "max" : 500,
    "workouts": [{
        "sets" : "2",
        "reps" : "5",
        "multiplier" : 0
      }, {
        "sets" : "1",
        "reps" : "5",
        "multiplier" : 0.5
      }, {
        "sets" : "1",
        "reps" : "3",
        "multiplier" : 0.7
      }, {
        "sets" : "1",
        "reps" : "2",
        "multiplier" : 0.9
      }, {
        "sets" : "5",
        "reps" : "5",
        "multiplier" : 1.0
      }]
    },{
    "name" : "Deadlift",
    "max" : 700,
    "workouts" : [{
        "sets" : "2",
        "reps" : "5",
        "multiplier" : 0.4
      }, {
        "sets" : "1",
        "reps" : "3",
        "multiplier" : 0.6
      }, {
        "sets" : "1",
        "reps" : "2",
        "multiplier" : 0.85
      }, {
        "sets" : "1",
        "reps" : "5",
        "multiplier" : 1.0
      }]
    },{
    "name" : "Overhead Press",
    "max" : 400, 
    "workouts" : [{
        "sets" : "2",
        "reps" : "5",
        "multiplier" : 0
      }, {
        "sets" : "1",
        "reps" : "5",
        "multiplier" : 0.55
      }, {
        "sets" : "1",
        "reps" : "3",
        "multiplier" : 0.7
      }, {
        "sets" : "1",
        "reps" : "2",
        "multiplier" : 0.85
      }, {
        "sets" : "5",
        "reps" : "5",
        "multiplier" : 1.0
      }]
    },{
    "name" : "Barbell Row",
    "max" : 300,
    "workouts": [{
        "sets" : "2",
        "reps" : "5",
        "multiplier" : 0.4
      }, {
        "sets" : "1",
        "reps" : "3",
        "multiplier" : 0.7
      }, {
        "sets" : "1",
        "reps" : "2",
        "multiplier" : 0.9
      }, {
        "sets" : "5",
        "reps" : "5",
        "multiplier" : 1.0
    }]
  }]
}
"""

@Serializable
data class Program(
    val exercises: List<ExercisesItem>?,
    val title: String = ""
) : Parcelable


@Serializable
data class WorkoutsItem(
    val reps: String = "",
    val sets: String = "",
    val multiplier: Float = 0f
) : Parcelable


@Serializable
data class ExercisesItem(
    val max: Int = 0,
    val workouts: List<WorkoutsItem>?,
    val name: String = ""
) : Parcelable


fun WorkoutsItem.calculate(weight: Float, barWeight: Float = 45f) : String {
    val result = weight * multiplier
    val digits: Long = 2
    val output = if (result > 0) {
        result.format(digits.toULong())
    } else {
        barWeight.format(digits.toULong())
    }
    return "${sets}x${reps} $output lbs"
}

fun Float.format(digits: ULong) = Platform().format(this, digits)
