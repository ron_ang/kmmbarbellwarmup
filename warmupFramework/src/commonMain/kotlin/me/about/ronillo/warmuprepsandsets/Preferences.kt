package me.about.ronillo.warmuprepsandsets

expect class SPref

expect fun SPref.get(key: String) : String
expect fun SPref.set(key: String, value: String)
