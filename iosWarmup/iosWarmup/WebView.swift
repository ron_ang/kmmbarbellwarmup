//
//  WebView.swift
//  iosWarmup
//
//  Created by Ronillo Ang on 7/12/21.
//  Copyright © 2021 Ronillo Ang. All rights reserved.
//

import SwiftUI
import WebKit

struct WebView : UIViewRepresentable {
    
    let request: URLRequest
    
    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
    
}
