//
//  ResultView.swift
//  iosWarmup
//
//  Created by Ronillo Ang on 7/11/21.
//  Copyright © 2021 Ronillo Ang. All rights reserved.
//

import SwiftUI
import warmupFramework

struct ResultView: View {

    var workoutItem: WorkoutsItem
    var weight: Float = 100

    var body: some View {
        let result = workoutItem.calculate(weight: self.weight, barWeight: 45)
        Text(String(result))
    }
}
