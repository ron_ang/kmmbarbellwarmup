//
//  WorkoutCalculatorView.swift
//  iosWarmup
//
//  Created by Ronillo Ang on 7/11/21.
//  Copyright © 2021 Ronillo Ang. All rights reserved.
//

import SwiftUI
import warmupFramework

struct WarmupCalculatorView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    private var btnBack : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
            Image("chevron.backward") // set image here
                .aspectRatio(contentMode: .fit)
                .foregroundColor(.white)
                Text("Go back")
            }
        }
    }

    var exercise: ExercisesItem?
    @State private var weightString: String = "100"

    var body: some View {
        ZStack(alignment: .top) {
            if (exercise == nil) {
                Text("Please select an exercise.").font(.title)
            } else {
                VStack {
                    btnBack
                    makeForm()
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }

    private func makeForm() -> some View {
        return VStack {
            Text(exercise!.name).font(.title2)

            Form {
                Text("Choose your working weight. Your warmup sets will then be calculated as well as the weights you need to put on each bar side.").padding()
                VStack(alignment: .leading) {
                    Text("Working weight:")
                    TextField("Weight", text: $weightString)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }.padding()
            }
            
            let weight = Float(weightString)
            
            if (weight != nil) {
                List {
                    Section(header: Text("Your warmup and working sets")) {
                        ForEach (exercise!.workouts!, id: \.multiplier) { workout in
                            ResultView(workoutItem: workout, weight: Float(weightString)!)
                        }
                    }
                }
            }
        }
    }
}
