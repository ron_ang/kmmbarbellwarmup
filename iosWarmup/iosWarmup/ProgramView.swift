import SwiftUI
import WebKit
import warmupFramework

struct ProgramView: View {

    @State private var barWeight = "45"
    @State private var exerciseItem: ExercisesItem? = nil
    
    private let program = ProgramSerializer().deserialize(jsonString: ProgramKt.PROGRAM_5X5_JSON)
    private let myYoutubePlaylist = URLRequest(url: URL(string: "https://www.youtube.com/watch?v=kaq_rWONjJg&list=PLLf9HfbPe25Z64b-zV0H3aU1sj_lVBP6l")!)
    private let aboutMeUrl = URLRequest(url: URL(string: "https://about.me/ronillo")!)
    private var idiom : UIUserInterfaceIdiom { UIDevice.current.userInterfaceIdiom }
    
    var body: some View {
        if (idiom == .pad) {
            padBody()
        } else {
            phoneBody()
        }
	}

    private func padBody() -> some View {
        return VStack {
            Text("5x5 Warmup").font(.title)

            HStack {
                masterView().frame(width: 300)
                detailView()
            }
            
            footerView()
        }
    }

    private func phoneBody() -> some View {
        return NavigationView {
            VStack {
                Text("5x5 Warmup").font(.title).padding()
                masterView()
                footerView()
            }.navigationBarTitle("").navigationBarHidden(true)
        }
    }

    private func masterView() -> some View {
        return VStack {
            List {
                ForEach (program.exercises!, id: \.name) { exerciseItem in
                    if idiom == .pad {
                        Button(exerciseItem.name) {
                            self.exerciseItem = exerciseItem
                        }
                    } else {
                        NavigationLink(exerciseItem.name, destination: WarmupCalculatorView(exercise: exerciseItem))
                    }
                }
            }

            Form {
                Text("Configure Bar Weight (in LB)").padding()
                VStack(alignment: .leading) {
                    TextField("Weight", text: $barWeight)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                }.padding()
            }
        }
    }
    
    private func detailView() -> some View {
        return WarmupCalculatorView(exercise: exerciseItem)
    }
    
    private func footerView() -> some View {
        return VStack {
            HStack {
                NavigationLink("Credits",
                               destination: WebView(request: aboutMeUrl))
                NavigationLink("Videos",
                               destination: WebView(request: myYoutubePlaylist))
            }
            Text("Powered by Kotlin Multiplatform").font(.footnote)
        }.padding()
    }
}
