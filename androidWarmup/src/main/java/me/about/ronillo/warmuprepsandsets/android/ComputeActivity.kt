package me.about.ronillo.warmuprepsandsets.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import me.about.ronillo.warmuprepsandsets.ExercisesItem
import me.about.ronillo.warmuprepsandsets.android.fragments.ComputeFragment

const val PARAM = "param"

class ComputeActivity : AppCompatActivity() {

    private lateinit var exerciseItem: ExercisesItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compute)

        val args =
            intent.extras ?: throw IllegalArgumentException("Compute Activity Requires Bundle")
        val exerciseItem =
            args.getSerializable(PARAM) ?: throw IllegalArgumentException("Missing arguments")
        this.exerciseItem = exerciseItem as ExercisesItem

        supportFragmentManager.beginTransaction().replace(
            R.id.container_fragment,
            ComputeFragment.newInstance(this.exerciseItem)
        ).commit()
    }
}