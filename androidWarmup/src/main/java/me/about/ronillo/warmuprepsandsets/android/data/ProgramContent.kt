package me.about.ronillo.warmuprepsandsets.android.data

import me.about.ronillo.warmuprepsandsets.ExercisesItem
import java.util.*
import kotlin.collections.MutableList
import kotlin.collections.MutableMap
import kotlin.collections.set

object ProgramContent {

    val ITEMS: MutableList<ExercisesItem> = ArrayList()
    val ITEM_MAP: MutableMap<String, ExercisesItem> = HashMap()

    fun addItem(item: ExercisesItem) {
        ITEMS.add(item)
        ITEM_MAP[item.name] = item
    }
}
