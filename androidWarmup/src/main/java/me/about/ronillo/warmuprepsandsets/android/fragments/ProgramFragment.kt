package me.about.ronillo.warmuprepsandsets.android.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.about.ronillo.warmuprepsandsets.PROGRAM_5X5_JSON
import me.about.ronillo.warmuprepsandsets.ProgramSerializer
import me.about.ronillo.warmuprepsandsets.android.R
import me.about.ronillo.warmuprepsandsets.android.data.MyProgramRecyclerViewAdapter
import me.about.ronillo.warmuprepsandsets.android.data.ProgramContent

class ProgramFragment : Fragment() {

    private var columnCount = 2
    private lateinit var rootView: View

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var itemClickListener: MyProgramRecyclerViewAdapter.OnExerciseItemClick? = null
        val programs = ProgramSerializer.deserialize(PROGRAM_5X5_JSON)

        ProgramContent.ITEMS.clear()
        programs.exercises?.forEach {
            ProgramContent.addItem(it)
        }

        if (activity is MyProgramRecyclerViewAdapter.OnExerciseItemClick) {
            itemClickListener = activity as MyProgramRecyclerViewAdapter.OnExerciseItemClick
        }

        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyProgramRecyclerViewAdapter(values =  ProgramContent.ITEMS,
                    itemClickListener = itemClickListener)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_program_list, container, false)
        return rootView
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setColumnCount(columnCount: Int) {
        if (columnCount >= 1) {
            this.columnCount = columnCount

            val view = rootView
            if (view is RecyclerView) {
                with(view) {
                    layoutManager = when {
                        columnCount <= 1 -> LinearLayoutManager(context)
                        else -> GridLayoutManager(context, columnCount)
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        }
    }
}