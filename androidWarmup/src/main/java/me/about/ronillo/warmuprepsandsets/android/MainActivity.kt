package me.about.ronillo.warmuprepsandsets.android

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import me.about.ronillo.warmuprepsandsets.ExercisesItem
import me.about.ronillo.warmuprepsandsets.android.data.MyProgramRecyclerViewAdapter
import me.about.ronillo.warmuprepsandsets.android.data.ProgramContent
import me.about.ronillo.warmuprepsandsets.android.fragments.ComputeFragment
import me.about.ronillo.warmuprepsandsets.android.fragments.ProgramFragment

class MainActivity : AppCompatActivity(), MyProgramRecyclerViewAdapter.OnExerciseItemClick {

    private var computeFragment: ComputeFragment? = null
    private var programFragment: ProgramFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.tool_bar))

        programFragment = supportFragmentManager.findFragmentById(R.id.fragment_program) as? ProgramFragment
        computeFragment = supportFragmentManager.findFragmentById(R.id.fragment_compute) as? ComputeFragment
    }

    override fun onResume() {
        super.onResume()

        if (computeFragment != null) {
            programFragment?.setColumnCount(1)
        }
    }

    fun onCreditsClick(v: View) {
        openWebPage("https://about.me/ronillo")
    }

    fun onVideosClick(v: View) {
        openWebPage("https://www.youtube.com/watch?v=kaq_rWONjJg&list=PLLf9HfbPe25Z64b-zV0H3aU1sj_lVBP6l")
    }

    fun onGithubClick(v: View) {
        openWebPage("https://bitbucket.org/ron_ang/kmmbarbellwarmup/")
    }

    private fun openWebPage(url: String) {
        val webpage = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onClick(exercisesItem: ExercisesItem) {
        if (computeFragment == null) {
            val intent = Intent(this, ComputeActivity::class.java)
            val args = Bundle()
            args.putSerializable(PARAM, exercisesItem)
            intent.putExtras(args)
            startActivity(intent)
        } else {
            computeFragment?.setExercise(exercisesItem)
            computeFragment?.refreshTitle()
        }
    }
}
