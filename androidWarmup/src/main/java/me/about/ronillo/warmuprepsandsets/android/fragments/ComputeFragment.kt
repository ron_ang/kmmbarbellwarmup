package me.about.ronillo.warmuprepsandsets.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import me.about.ronillo.warmuprepsandsets.ExercisesItem
import me.about.ronillo.warmuprepsandsets.KMMStorage
import me.about.ronillo.warmuprepsandsets.android.R
import me.about.ronillo.warmuprepsandsets.calculate

class ComputeFragment : Fragment() {

    private var exerciseItem: ExercisesItem? = null
    private lateinit var rootView: View
    private lateinit var kmmStorage: KMMStorage

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.fragment_compute, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        kmmStorage = KMMStorage(requireActivity())

        val computeButton = rootView.findViewById<Button>(R.id.button_compute)
        val inputLayout = rootView.findViewById<TextInputLayout>(R.id.input_weight_layout)
        val input = rootView.findViewById<TextInputEditText>(R.id.input_weight)

        computeButton.setOnClickListener {
            inputLayout.error = null
            try {
                val weight = input.text.toString().toFloat()
                compute(weight)
            } catch (e: NumberFormatException) {
                inputLayout.error = e.message
            }
        }
    }

    override fun onResume() {
        super.onResume()
        refreshTitle()
    }

    fun refreshTitle() {
        val title = rootView.findViewById<TextView>(R.id.label_title)
        title.text = exerciseItem?.name
    }

    fun setExercise(exerciseItem: ExercisesItem) {
        this.exerciseItem = exerciseItem
    }

    private fun compute(weight: Float) {
        if (exerciseItem == null) return

        val inflater = LayoutInflater.from(requireActivity())
        val list = rootView.findViewById<LinearLayout>(R.id.warmup_list)
        val value = kmmStorage.get("bar-weight")
        list.removeAllViews()

        exerciseItem?.workouts?.forEach {
            val itemView = inflater.inflate(R.layout.listitem_compute, list, false)
            val content = itemView.findViewById<TextView>(R.id.content)
            var barWeight = 45f

            try {
                barWeight = value.toFloat()
            } catch (e: NumberFormatException) {
            }

            content.text = it.calculate(weight, barWeight)

            list.addView(itemView)
        }
    }

    companion object {

        fun newInstance(exerciseItem: ExercisesItem): ComputeFragment {
            val fragment = ComputeFragment()
            fragment.setExercise(exerciseItem)
            return fragment
        }
    }
}
