package me.about.ronillo.warmuprepsandsets.android.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import me.about.ronillo.warmuprepsandsets.KMMStorage
import me.about.ronillo.warmuprepsandsets.android.R

class ConfigureBarWeightFragment : Fragment() {

    private lateinit var kmmStorage: KMMStorage
    private lateinit var inputWeight: TextView
    private lateinit var inputWeightLayout: TextInputLayout
    private lateinit var saveButton: Button
    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.fragment_configure_bar_weight, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        kmmStorage = KMMStorage(requireActivity())
        inputWeight = rootView.findViewById(R.id.input_bar_weight)
        inputWeightLayout = rootView.findViewById(R.id.input_bar_weight_layout)
        saveButton = rootView.findViewById(R.id.button_save_bar_weight)
        inputWeight.text = kmmStorage.get("bar-weight")
        saveButton.setOnClickListener {
            val input = inputWeight.text.toString()

            if (input.isNotEmpty()) {
                try {
                    input.toFloat()
                    kmmStorage.put("bar-weight", input.format("%.2f"))
                } catch (e: NumberFormatException) {
                    inputWeightLayout.error = e.message
                }
            }
        }
    }
}
