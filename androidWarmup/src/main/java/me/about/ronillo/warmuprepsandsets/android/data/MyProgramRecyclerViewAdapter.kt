package me.about.ronillo.warmuprepsandsets.android.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.about.ronillo.warmuprepsandsets.ExercisesItem
import me.about.ronillo.warmuprepsandsets.android.databinding.FragmentProgramBinding

class MyProgramRecyclerViewAdapter(
    private val values: List<ExercisesItem>,
    private val itemClickListener: OnExerciseItemClick? = null
) : RecyclerView.Adapter<MyProgramRecyclerViewAdapter.ViewHolder>() {

    interface OnExerciseItemClick {

        fun onClick(exercisesItem: ExercisesItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentProgramBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = (position + 1).toString()
        holder.contentView.text = item.name
        holder.bindClickListener {
            itemClickListener?.onClick(item)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentProgramBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }

        fun bindClickListener(onClickListener: View.OnClickListener) {
            itemView.setOnClickListener(onClickListener)
        }
    }
}