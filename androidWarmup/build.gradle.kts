plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android")
}

dependencies {
    implementation(project(":warmupFramework"))
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.appcompat:appcompat:1.3.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("com.intuit.sdp:sdp-android:1.0.6")
}

android {
    compileSdkVersion(30)
    defaultConfig {
        applicationId = "me.about.ronillo.warmuprepsandsets.android"
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 2
        versionName = "1.1"

        base {
            archivesBaseName = "StrongLifts-Warmup-v$versionName-$versionCode"
        }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    buildFeatures {
        viewBinding = true
    }
}