# README #

This README would normally document whatever steps are necessary to get this application up and running.
I assume that you are already knowledgeable about the Kotlin Multiplatform Mobile components, else [learn it](https://kotlinlang.org/docs/mobile/getting-started.html).

### Download The App ###
[Android App](https://play.google.com/store/apps/details?id=me.about.ronillo.warmuprepsandsets.android)

Unfortunately, I do not have an App Store developer account yet because its too expensive for a guy like me whose only developing free apps.


### What is this repository for? ###

* This repo contains the source code for the Barbell Warmup mobile application.
* 1.0


### How do I get set up? ###

* Install Android Studio Arctic Fox
* Install XCode 12.4
* Install CocoaPods 1.8.4
* Open the project


### App UI ###

![Tablet](https://bitbucket.org/ron_ang/kmmbarbellwarmup/downloads/tablet-looks.png)

![iPad](https://bitbucket.org/ron_ang/kmmbarbellwarmup/downloads/ipad-looks.png)

![Initial Landing](https://bitbucket.org/ron_ang/kmmbarbellwarmup/downloads/Barbell_Warmup_Home.jpg)



* More images of the UI is committed in the root folder of this project.

### Contribution guidelines ###

* Unit testing
* Code review


### Who do I talk to? ###

* Repo owner or admin
* [LinkedIn](https://linkedin.com/in/ronillo-ang)